"""Convenience wrapper for running bootstrap directly from source tree."""


from sumext.sumext import main


if __name__ == '__main__':
    main()
