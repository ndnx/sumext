"""Sumext setup file."""
from setuptools import setup


setup(
    name='Sumext',
    version='0.0.1',
    py_modules=[],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        sumext=sumext:cli
    '''
)
