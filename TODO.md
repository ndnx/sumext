TODO List (~30%)
==================
* Similarity module
    * Word similarity function
        * ~~IS-A Tree method~~
        * HAS-A Tree method
    * ~~Semantic similarity function~~ **(Done)**
        * ~~Lexical semantic vector creation~~
        * ~~Weighting using corpus statistic~~
        * ~~Calculating the semantic similarity~~
    * Word order similarity function
        * Word order vector creation
        * Calculating the word order similarity
    * Overall similarity function
* ~~Corpus module~~ **(Done)**
    * ~~Find the corpus~~
    * ~~Create corpus class~~
        * ~~Corpus retrieval function~~
        * ~~Corpus statistic function~~
* Summary module *(Main module)*
    * Graph creation function
        * Sentence tokenization and vertex creation
        * Edge calculation
    * Graph iteration function
    * Summary creation function

Problem List
==================
* Example result is different from the paper
* Still searching for a way to calculate HAS-A relation (holonym and meronym) from WordNet
* Need more corpus to reduce the bias (for now still don't know what corpus to use)
* Don't know if the Python implementation is correct or not (coding method, style, etc.), the program runtime is still slow
