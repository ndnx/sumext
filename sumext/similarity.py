from nltk.corpus import wordnet
from nltk.tokenize import word_tokenize
from math import exp, log, sqrt
from itertools import product
from collections import namedtuple
from corpus import Corpus


# Initiate our _constants
Constants = namedtuple('Constants', ['alpha', 'beta', 'threshold'])
_constants = Constants(0.2, 0.45, 0.2)
_corpus = Corpus()


def word_similarity(word1, word2, language="ind"):
    """Return the distance between two words."""
    # Find all synset of two words.
    sets1 = wordnet.synsets(word1, lang=language)
    sets2 = wordnet.synsets(word2, lang=language)

    # Return 0 if one of the set is empty
    if(not sets1 or not sets2):
        return 0

    # Find the word sense based on minimum path length.
    # For now still using IS-A relationship tree.
    WSet = namedtuple('WSet', 'word1 word2 path_length')
    results = []
    for x in product(sets1, sets2):
        # Use simulate_root = True to prevent path_distance = None
        # results.append(WSet(x[0], x[1], x[0].shortest_path_distance(x[1],
        #                     simulate_root=True)))
        results.append(WSet(x[0], x[1], x[0].shortest_path_distance(x[1])))

    # print('\n'.join('{}: {}'.format(*k) for k in enumerate(results)))

    # If all of the path_length is None, return 0
    if([l.path_length for l in results] == [None] * len(results)):
        return 0

    # Get the word_set with minimum path_length
    word_set = min((x for x in results if x.path_length is not None),
                   key=lambda y: y.path_length)

    # Find the depth of lowest common subsumer of two synsets
    H = max(word_set.word1
            .lowest_common_hypernyms(word_set.word2, simulate_root=True)) \
        .max_depth()

    # Check the condition
    if word_set.word1 == word_set.word2:
        L = 0
    elif not set(word_set.word1.lemma_names()) \
            .isdisjoint(word_set.word2.lemma_names()):
        L = 1
    else:
        L = word_set.path_length

    # Calculate the f(l)
    fL = exp(-(_constants.alpha*L))

    # Calculate the f(h)
    fH = ((exp(_constants.beta*H))-(exp(-(_constants.beta*H)))) / \
        ((exp(_constants.beta*H))+(exp(-(_constants.beta*H))))

    return(fL*fH)


def semantic_similarity(sentence1, sentence2):
    """Return the semantic distance between two sentences."""
    # Tokenize the sentences and create the union between them.
    t1 = word_tokenize(sentence1.lower())
    t2 = word_tokenize(sentence2.lower())
    t = list(set(t1 + t2))

    # Create the lexical semantic vector
    lsvt1 = _lexical_semantic_vector(t1, t)
    lsvt2 = _lexical_semantic_vector(t2, t)

    # Return the cosine similarity of two vector
    return _cosine_similarity(lsvt1, lsvt2)


def _lexical_semantic_vector(sentence, u_sentence, language="ind"):
    """Return the lexical semantic vector for one sentence."""
    # Variable initialization
    columns = range(len(u_sentence))

    # Create matrix m rows and n columns to hold our value
    # (to access, lsm[m][n])
    lsv = [0 for col in columns]
    iv = [0 for col in columns]
    for c_idx, c_val in enumerate(u_sentence):
        tmp = 0
        wsets = [c_val, c_val]
        for r_idx, r_val in enumerate(sentence):
            if(r_val == c_val):
                tmp = 1
                wsets = [sentence[r_idx], u_sentence[c_idx]]
                break
            else:
                # print str(r_val) + ' ' + str(c_val) + '\n'
                w_sim = word_similarity(r_val, c_val, language)
                if(w_sim > _constants.threshold and w_sim >= tmp):
                    tmp = w_sim
                    wsets = [sentence[r_idx], u_sentence[c_idx]]
            # print c_val + ' ' + r_val

        # print str(wsets) + '\n'
        if wsets:
            wrestat = 1
            for w in wsets:
                wstat = 1-(log(_corpus.freq(w)+1)/log(_corpus.total()+1))
                wrestat *= wstat
            iv[c_idx] = wrestat
            # print str(wrestat) + '\n'
        lsv[c_idx] = tmp

    # Find the maximum value for each column
    # lsv = [(max(c[x] for c in lsm)) for x in columns]

    return [a*b for a, b in zip(lsv, iv)]


def word_order_similarity(sentence1, sentence2):
    """Return word order distance between two sentences."""
    pass


def _cosine_similarity(v1, v2):
    """compute cosine similarity of v1 to v2.

    (v1 dot v2)/{||v1||*||v2||)
    """
    sumxx, sumxy, sumyy = 0, 0, 0
    for i in range(len(v1)):
        x = v1[i]
        y = v2[i]
        sumxx += x*x
        sumyy += y*y
        sumxy += x*y
    return sumxy/sqrt(sumxx*sumyy)
