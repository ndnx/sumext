"""Read the corpus."""
import csv
from collections import Counter


class Corpus(object):
    """Corpus object."""

    def __init__(self):
        """Initialize the class."""
        cl = 'resources\corpus\Indonesian_Manually_Tagged_Corpus.tsv'
        with open(cl, 'rb') as cf:
            reader = csv.reader(cf, dialect="excel-tab",
                                quoting=csv.QUOTE_NONE)
            cr = tuple((x[0].lower(), x[1].lower()) for x in reader if x)
        self.words = cr

    def words(self):
        """Return the word lists with its POS TAGs."""
        return self.words

    def word_freqs(self, words=None):
        """Return the frequencies of the input words (case insensitive).

        If no input specified, return all the list, else return the list of the
        specified input (list of words)
        """
        freqs = Counter([x[0] for x in self.words])

        if not words:
            return freqs

        w = [i.lower() for i in words]
        return Counter({j: freqs[j] for j in w})

    def freq(self, word):
        """Return the frequency of specified word (case insensitive)."""
        return self.word_freqs()[word.lower()]

    def total(self):
        """Return the total of corpus words."""
        return sum(self.word_freqs().values())
