"""Extracting summary from txt document."""

import click
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

__version__ = "0.0.1"


@click.command()
@click.option(
    '--input',
    '-i',
    help='Document file (must be txt file)',
    type=click.Path(exists=True, readable=True, resolve_path=True),
    required=True
)
def main(input):
    """Create a summary given document input and put it in database."""
    test_t = click.open_file(input, 'r')
    test = stem(test_t.read())
    print test


def stem(input):
    """Stem the input string."""
    # create stemmer
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()

    return stemmer.stem(input)
