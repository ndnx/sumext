"""The summary algorithm."""

from nltk.corpus import wordnet as wn


def create_summary(str_in):
    """The main function of summary."""
    # Create the node or vertex filled with sentence
    vertex = str_in.split('.')
    total_sentence = len(vertex)

    # Create matrix representation of edge filled with zeroes
    edge = [[0 for x in range(total_sentence)] for x in range(total_sentence)]


def _sentence_similarity(sentence_1, sentence_2):
    """Sentence similarity, return value between 0 and 1."""
    pass


def _word_similarity(word_1, word_2):
    """Word similarity, return value between 0 and 1."""
    # calculate the words path length
    pass


def _is_synonym(word_1, word_2):
    """Return True if word in the same synset, else return False."""
    for s in wn.synsets(word_1):
        lemmas = s.lemmas()
        for l in lemmas:
            if l.name() == word_2:
                return True
    return False


def _clean_string(str_in):
    """Clean the sentence from punctuation."""
    # define punctuation
    punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''

    # remove punctuation from the string
    no_punct = ""
    for char in str_in:
        if char not in punctuations:
            no_punct = no_punct + char

    # display the unpunctuated string
    return no_punct
